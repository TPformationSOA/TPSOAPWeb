package org.universite.ws.soapClient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan("org.universite.ws")
public class GestionInscriptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionInscriptionApplication.class, args);
	}
}
