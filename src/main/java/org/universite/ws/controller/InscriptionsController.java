package org.universite.ws.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.universite.interfaces.etudiant.v1.model.EtudiantType;
import org.universite.interfaces.gestioninscriptions.v2.ErreurTechniqueFault;
import org.universite.interfaces.gestioninscriptions.v2.GererInscriptionSoap;
import org.universite.interfaces.gestioninscriptions.v2.GererInscriptionSoapService;

@Controller
public class InscriptionsController {

 
        // Renvoie ver la page avec le tableau contenant la liste des inscrits
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String listeInscriptions(Model model) throws MalformedURLException {
            // A COMPLETER : r�cup�rer liste des �tudiants inscrits via le service web
            model.addAttribute("listeInscrits", etudiants);
	    return "listeInscriptions";
	}
        
        // Initialise un objet Etudiant pour qu'il soit renseign� par le formulaire
	@RequestMapping(value="/inscriptions", method=RequestMethod.GET)
 	public String afficheFormulaire(Map<String, Object> model) {
                EtudiantType etudiant= new EtudiantType();
                model.put("etudiant",etudiant);
                return "formInscriptions";
        }
        
        
        // Recoit les donn�es de l'�tudiant du formulaire et donc
        // cr�e l'inscription via appel WS  et renvoie vers la page d'affichage de la liste
        // Noter l'usage du redirect
         @RequestMapping(value = "/inscriptions", method = RequestMethod.POST)
        public String updateContact(EtudiantType etudiant, Model model) {
            // Invoquer le service web pour inscrire l'�tudiant
            return "redirect:/";
        }
}
