# Objectifs

* Développer une application utilisant le client Java pour invoquer le service Web
* Réaliser une application web avec un formulaire d'inscription HTML et une page d'affichage des inscrits dans un 
tableau en utilisant également le client java pour invoquer le service web

## Pré-requis
Reprendre la structure du projet Server, ou utiliser ce projet GIT pour disposer des WSDL et du fichier POM
avec les dépendances SpringBoot

# Programme Java client du service web

* Modifier le POM pour générer non plus le serveur mais le client et une implémentation exemple

    `<extraarg>-client</extraarg>
     <extraarg>-impl</extraarg> `

* Recopier l'implémentation générée dans votre package de source et modifier le programme
pour invoquer le service avec des exemples

* Tester via SOAP-UI et via votre programme

# Application web (avec client Java du WebService SOAP)

Vous pouvez utiliser une techno web que vous maitrisez ou ce projet basé sur Spring MVC et Thymeleaf

* Externaliser l'url du service web dans le fichier application et récupérer le dans le code via

    `@Value("${urlWS}")
     private String url; `

* Compléter la classe InscriptionsController pour invoquer les services attendues

* Lancer l'application et tester via navigateur

NB : Le serveur doit bien entendu être lancé

* Modifier l'url dans applicatio.yml pour invoquer le service d'un collègue











